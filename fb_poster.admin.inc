<?php

/**
 * @file
 * Contains fb_poster settings forms.
 */

/**
 * Page callback.
 * Return form with fb_poster app settings.
 */
function fb_poster_app_setting_form() {

  $form = array();

  $form['fb_poster'] = array(
    '#type' => 'fieldset',
    '#title' => t('Application settings'),
  );

  $form['fb_poster']['app_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Facebook App ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('fb_poster_app_id'),
    '#description' => t('Application ID.'),
  );

  $form['fb_poster']['app_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#required' => TRUE,
    '#default_value' => variable_get('fb_poster_app_secret'),
    '#description' => t('Application secret key.'),
  );

  $form['fb_poster']['state_parameter'] = array(
    '#type' => 'textfield',
    '#title' => t('State parameter'),
    '#required' => TRUE,
    '#default_value' => variable_get('fb_poster_state_parameter'),
    '#description' => t('Use of the state parameter helps to guard against Cross-site Request Forgery.'),
  );

  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );

  return $form;
}

/**
 * Submit callback for fb_poster app settings form.
 */
function fb_poster_app_setting_form_submit($form, &$form_state) {
  global $base_url;

  $pre_app_id = variable_get('fb_poster_app_id');
  $pre_app_secret = variable_get('fb_poster_app_secret');
  $pre_state_parameter = variable_get('fb_poster_state_parameter');

  $app_id = $form_state['values']['app_id'];
  $app_secret = $form_state['values']['app_secret'];
  $state_parameter = $form_state['values']['state_parameter'];

  if ($pre_app_id != $app_id || $pre_app_secret != $app_secret || $pre_state_parameter != $state_parameter) {
    $redirect_url = $base_url . FB_POSTER_REDIRECT_URL;

    $permissions = 'manage_pages,publish_stream,offline_access';

    variable_set('fb_poster_app_id', $app_id);
    variable_set('fb_poster_app_secret', $app_secret);
    variable_set('fb_poster_state_parameter', $state_parameter);

    $path = "https://www.facebook.com/dialog/oauth?client_id=$app_id&redirect_uri=$redirect_url&scope=$permissions&state=$state_parameter";
    drupal_goto($path);
  }
}

/**
 * Page callback.
 * Return form with fb_poster post settings.
 */
function fb_poster_post_setting_form() {
  $user_info = variable_get('fb_poster_user_info');
  $pages_info = variable_get('fb_poster_pages_info');

  $form = array();

  $form['fb_poster'] = array(
    '#type' => 'fieldset',
    '#title' => t('Post settings'),
  );

  if (empty($user_info) && empty($pages_info)) {
    $form['fb_poster']['fb_poster_no_page'] = array(
      '#markup' => t('You have not configured the application. Go to !url.', array('!url' => l(t('Facebook CrossPoster application settings'), 'admin/config/services/fb_poster'))),
    );
  }
  else {
    $user_info = unserialize($user_info);
    $pages_info = unserialize($pages_info);
    $access_token = variable_get('fb_poster_access_token_me');

    $options = array(
      'options' => array(),
      'targets' => array(),
    );

    $options['options'][$user_info->id] = $user_info->name;
    $options['targets'][$user_info->id] = $access_token;

    foreach($pages_info as $page) {
      if ($page->category != 'Application') {
        $options['options'][$page->id] = $page->name;
        $options['targets'][$page->id] = $page->access_token;
      }
    }

    variable_set('fb_poster_targets', $options['targets']);

    $form['fb_poster']['fb_poster_target_id'] = array(
      '#type' => 'select',
      '#title' => t('Target page'),
      '#options' => $options['options'],
      '#default_value' => variable_get('fb_poster_target_id'),
    );

    $form['fb_poster']['fb_poster_author'] = array(
      '#type' => 'checkbox',
      '#title' => t('Write from group name'),
      '#default_value' => variable_get('fb_poster_author', 0),
      '#description' => t('Check this if you want to post messages from group name'),
      '#states' => array(
        'invisible' => array(
          ':input[name="fb_poster_target_id"]' => array('value' => $user_info->id),
        ),
        'unchecked' => array(
          ':input[name="fb_poster_target_id"]' => array('value' => $user_info->id),
        ),
      ),
    );

    $form['actions']['save'] = array(
      '#type' => 'submit',
      '#value' => t('Save configuration'),
    );
  }

  return $form;
}

/**
 * Submit callback for fb_poster post settings form.
 */
function fb_poster_post_setting_form_submit($form, &$form_state) {
  $targets = variable_get('fb_poster_targets');
  $author = $form_state['values']['fb_poster_author'];
  $target_id = $form_state['values']['fb_poster_target_id'];
  $access_token = $targets[$target_id];

  variable_set('fb_poster_author', $author);
  variable_set('fb_poster_target_id', $target_id);
  variable_set('fb_poster_access_token', $access_token);

  drupal_set_message(t('The configuration options have been saved.'), 'status');
}

/**
 * Page callback.
 * Return form with fb_poster node settings.
 */
function fb_poster_node_settings() {

  $form = array();

  $form['fb_poster'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node settings'),
  );

  $form['fb_poster']['fb_poster_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types to post'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('fb_poster_node_types', array()),
  );

  $form['fb_poster_body'] = array(
    '#type' => 'fieldset',
    '#title' => t('Message'),
  );

  $form['fb_poster_body']['fb_poster_message_format'] = array(
    '#type' => 'select',
    '#title' => t('Format'),
    '#options' => array(
      'default' => t('Default'),
      'trimmed' => t('Trimmed'),
    ),
    '#default_value' => variable_get('fb_poster_message_format', 'default'),
  );

  $form['fb_poster_body']['fb_poster_trim_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Trim length'),
    '#size' => 4,
    '#default_value' => variable_get('fb_poster_trim_length', 600),
    '#states' => array(
      'visible' => array(
        ':input[name="fb_poster_message_format"]' => array('value' => 'trimmed'),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Return form with fb_poster images settings.
 */
function fb_poster_images_settings() {
  $form = array();

  $form['fb_poster'] = array(
    '#type' => 'fieldset',
    '#title' => t('Images settings'),
  );

  // Get node types that should be posted.
  $selected_node_types = _fb_poster_get_selected_node_types();
  if (!$selected_node_types) {
    $form['fb_poster']['image'] = array(
      '#markup' => t('Please, go to !url and select content types to see their image fiels', array('!url' => l(t('Node settings page'), 'admin/config/services/fb_poster/node'))),
    );
    return $form;
  }

  if (module_exists('image')) {
    $image_fields = array();
    $fields_info = field_info_fields();
    foreach ($selected_node_types as $node_type) {

      // Build fieldset for every selected node type.
      $node_types = node_type_get_names();
      $form['fb_poster']['fb_poster_node_type_' . $node_type] = array(
        '#type' => 'fieldset',
        '#title' => $node_types[$node_type],
      );

      foreach ($fields_info as $field) {
        if ($field['type'] == 'image' && isset($field['bundles']['node'])) {
          $bundles = $field['bundles']['node'];
          if (in_array($node_type, $bundles)) {
            $image_fields[$node_type][$field['field_name']] = $field['field_name'];
          }
        }
      }

      // If imagefield for this node type found - build settings form for it.
      if (isset($image_fields[$node_type])) {
        array_unshift($image_fields[$node_type], t('Hidden'));

        $form['fb_poster']['fb_poster_node_type_' . $node_type]['fb_poster_' . $node_type . '_image_field'] = array(
          '#type' => 'select',
          '#title' => t('Select imagefield'),
          '#options' => $image_fields[$node_type],
          '#default_value' => variable_get('fb_poster_' . $node_type . '_image_field', '0'),
          '#description' => t('Selected field will be posted with message to facebook.com'),
        );
      }
      else {
        $form['fb_poster']['fb_poster_node_type_' . $node_type]['empty_value'] = array('#markup' => t('This node types does not contain image fields'));
      }
    }
  }
  else {
    $form['fb_poster']['image'] = array('#markup' => t("You can't post images until IMAGE module is installed."));
    return $form;
  }

  return system_settings_form($form);
}
