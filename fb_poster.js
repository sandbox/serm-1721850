(function ($) {

Drupal.behaviors.fb_poster = {
  attach: function (context) {
    $('fieldset#edit-fb-poster-node', context).drupalSetSummary(function(context) {
      var vals = [];
      if ($('#edit-fb-poster', context).is(':checked')) {
        vals.push(Drupal.t('Post this node to facebook.com : Yes'));
      }
      else {
        vals.push(Drupal.t('Post this node to facebook.com : No'));
      }
      return vals.join(', ');
    });
  }
};

})(jQuery);
