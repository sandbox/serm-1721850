﻿Install
=======

1) Go to developers page and create application.

2) Enable module on your site

3) You need to download the http://github.com/facebook/facebook-php-sdk and extract the entire contents of the archive into the sites/all/libraries/facebook-php-sdk folder of your server.

4) Make sure you have permission for the users of the site to crosspost or post any nodes 

5) Go to application settings module, insert the ID of the Application and his secret code and the State parameter (you should invent it yourself, it is necessary for security) in the appropriate fields. Click “Save” and give  the application access to your data.

6) Go to the page with the module publication settings, select the target page. If the target page is a group and you want to publish something on its behalf, you should put “tick” in the appropriate checkbox. Note: you need to visit that page at least once and press “Save”.

7) Go to the settings of content types for publication and select the necessary type.

8) If you want to attach a picture, go to the page with the image settings and select the required fields.


Usage
=====

When creating or editing node, just check "Post this node to facebook.com" and data will be automatically sent to facebook.com. For posting images Image module should be enabled.
